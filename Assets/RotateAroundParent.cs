﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAroundParent : MonoBehaviour {

    public Vector3 m_rotationDirection;
    public float m_speedMin = 10;
    public float m_speedMax = 90;


    public Transform[] m_objects;
    public List<Rotator> m_rotators = new List<Rotator>();

    

    void Start() {
        foreach (var obj in m_objects)
        {

            m_rotators.Add(new Rotator() { m_affected = obj, m_direction = new Vector3(GetRandom(), GetRandom(), GetRandom()) });
        }
        
    }

    private float GetRandom()
    {
        return UnityEngine.Random.Range(-m_speedMin, m_speedMax);
    }
    public void Update()
    {
        Rotator rot;
        for (int i = 0; i < m_rotators.Count; i++)
        {
            rot = m_rotators[i];
            rot.m_affected.RotateAround(transform.position, rot.m_direction, 1);

        }
    }

    public class  Rotator {
        public Transform m_affected;
        public Vector3 m_direction;
    }
}
