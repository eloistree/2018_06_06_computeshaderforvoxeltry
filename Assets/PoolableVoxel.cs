﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IPoolable {

    void Hide();
    void Display();

    void Claim();
    void Free();

}

public class PoolableVoxel : MonoBehaviour , IPoolable{


    [Header("Claimm")]
    public UnityEvent m_onClaim;
    public UnityEvent m_onFree;

    [Header("Display")]
    public UnityEvent m_onHide;
    public UnityEvent m_onDisplay;

    public void Claim()
    {
    //    throw new System.NotImplementedException();
    }

    public void Display()
    {
      //  throw new System.NotImplementedException();
    }

    public void Free()
    {
     //   throw new System.NotImplementedException();
    }

    public void Hide()
    {
      //  throw new System.NotImplementedException();
    }
}
