﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseVoxelShader : MonoBehaviour {

    public ComputeShader shader;
    
    public Transform[] voxel;

    private int[] voxelsState;
    private Vector3[] colorsState;

    [Header("Next To implement")]
    public Color[] previousColor;
    public Color[] currentColor;

    public float speed=3;
    //[Header("Debug")]
    //public Renderer rend;
    //public RenderTexture myRT;
    // Use this for initialization
    void Start ()
    {
        colorsState = new Vector3[voxel.Length];
        voxelsState = new int[voxel.Length];
        //myRT = new RenderTexture(textureResolution,textureResolution,24);
        //myRT.enableRandomWrite = true;
        //myRT.Create();

        //rend = GetComponent<Renderer>();
        //rend.enabled = true;

        RandomBools();

        UpdateTextureFromCompute();
        int sqrt =(int) System.Math.Pow(voxel.Length, 1f / 3f);
        int index = 0;
        for (int x = 0; x < sqrt; x++)
        {
            for (int y = 0; y < sqrt; y++)
            {
                for (int z = 0; z < sqrt; z++)
                {

                    voxel[index].localPosition = new Vector3(x, y, z);
                    index++;
                }
            }
        }

        
    }

    private void RandomBools()
    {
        for (int i = 0; i < colorsState.Length; i++)
        {
            voxelsState[i] = UnityEngine.Random.Range(0,2)==0?1:0;
        }
    }

    // Update is called once per frame
    void UpdateTextureFromCompute () {
        int kernetHandle = shader.FindKernel("CSMain");
        
        shader.SetFloat("DeltaTime", Time.deltaTime* speed);

        ComputeBuffer buffer = new ComputeBuffer(colorsState.Length, 12);
        buffer.SetData(colorsState);
        shader.SetBuffer(kernetHandle, "output", buffer);

        ComputeBuffer bufferbool = new ComputeBuffer(voxelsState.Length, 32);
        bufferbool.SetData(voxelsState);
        shader.SetBuffer(kernetHandle, "voxel", bufferbool);

        shader.Dispatch(kernetHandle, voxel.Length, 1, 1);


      //  colorsState = new Vector3[colorsState.Length];
        buffer.GetData(colorsState);
        buffer.Dispose();
        bufferbool.Dispose();

    }
    

    public void Update()
    {
       
            UpdateTextureFromCompute();

        for (int i = 0; i < voxel.Length; i++)
        {
            voxel[i].localScale = colorsState[i];
        }

        if (Input.GetKeyDown(KeyCode.R))
            RandomBools();
    }

    //public void OnValidate()
    //{
    //    colorsState = new Vector3[voxel.Length];
    //    voxelsState = new int[voxel.Length];
    //}
}
